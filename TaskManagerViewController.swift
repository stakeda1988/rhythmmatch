//
//  TaskManagerViewController.swift
//  TaskManager
//
//  Created by Ravi Shankar on 12/07/14.
//  Copyright (c) 2014 Ravi Shankar. All rights reserved.
//

import UIKit
import CoreData
import iAd

class TaskManagerViewController: UITableViewController, NSFetchedResultsControllerDelegate, NADViewDelegate {
    @IBOutlet weak var resultLabel: UIButton!

    let managedObjectContext = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
    private var nadView: NADView!
    var globalAdjustedSynchroPersent:Double!
    var selectedCount:Int = 0
    var selectedCell1:String = ""
    var selectedCell2:String = ""
    var selectedArray1:[String] = []
    var selectedArray2:[String] = []
    var synchro1:Double = 0
    var synchro2:Double = 0
    var synchro3:Double = 0
    var synchro4:Double = 0
    var synchro5:Double = 0
    var synchro6:Double = 0
    var synchro7:Double = 0
    var synchro8:Double = 0
    var synchro9:Double = 0
    var synchroPersent:Double = 0
    var fetchedResultController: NSFetchedResultsController = NSFetchedResultsController()
    var delegate:AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    var adTimer:NSTimer!
    override func viewDidLoad() {
        super.viewDidLoad()
        let myBoundSize: CGSize = UIScreen.mainScreen().bounds.size
        nadView = NADView(frame: CGRect(x: myBoundSize.width/2-160, y: myBoundSize.height-50, width: 320, height: 50))
        nadView.setNendID("3df85edb7a0b0b30ed826a296a0c0881a8612fe1",
            spotID: "492647")
        nadView.isOutputLog = false
        nadView.delegate = self
        nadView.load()
        self.view.addSubview(nadView)
        adTimer = NSTimer.scheduledTimerWithTimeInterval(10.0, target: self, selector: Selector("onAdStart"), userInfo: nil, repeats: true)
//        self.canDisplayBannerAds = true
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 246/255, green: 241/255, blue: 227/255, alpha: 0.5)
        fetchedResultController = getFetchedResultController()
        fetchedResultController.delegate = self
        do {
            try fetchedResultController.performFetch()
        } catch _ {
        }
//        tableView.rowHeight = 100
        tableView.backgroundView = nil
        tableView.backgroundColor = UIColor.clearColor()
        tableView.separatorColor = UIColor.clearColor()
//        let myBoundSize: CGSize = UIScreen.mainScreen().bounds.size
        let imageView = UIImageView(frame: CGRectMake(0, 0, myBoundSize.width, myBoundSize.height))
        let image = UIImage(named: "04.jpg")
        imageView.image = image
        imageView.alpha = 1.0
        tableView.backgroundView = imageView
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        if segue.identifier == "edit" {
            let cell = sender as! UITableViewCell
            let indexPath = tableView.indexPathForCell(cell)
            let taskController:TaskDetailViewController = segue.destinationViewController as! TaskDetailViewController
            let task:Tasks = fetchedResultController.objectAtIndexPath(indexPath!) as! Tasks
            taskController.task = task
        }else if segue.identifier == "result" {
            let taskController:ResultViewController = segue.destinationViewController as! ResultViewController
            taskController.resultSynchroRate = globalAdjustedSynchroPersent
        }
    }
    
    func getFetchedResultController() -> NSFetchedResultsController {
        fetchedResultController = NSFetchedResultsController(fetchRequest: taskFetchRequest(), managedObjectContext: managedObjectContext!, sectionNameKeyPath: nil, cacheName: nil)
        return fetchedResultController
    }
    
    func taskFetchRequest() -> NSFetchRequest {
        let fetchRequest = NSFetchRequest(entityName: "Tasks")
        let sortDescriptor = NSSortDescriptor(key: "desc", ascending: true)
        fetchRequest.sortDescriptors = [sortDescriptor]
        return fetchRequest
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        let numberOfSections = fetchedResultController.sections?.count
        return numberOfSections!
        
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let numberOfRowsInSection = fetchedResultController.sections?[section].numberOfObjects
        return numberOfRowsInSection!
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) 

//            tableView.rowHeight = 100
            tableView.allowsMultipleSelection = true
        
            let task = fetchedResultController.objectAtIndexPath(indexPath) as! Tasks
            var tmpDouble1:NSString = NSString(format:"%.2f",task.interval1) as String;
            var tmpDouble2:NSString = NSString(format:"%.2f",task.interval2) as String;
            var tmpDouble3:NSString = NSString(format:"%.2f",task.interval3) as String;
            var tmpDouble4:NSString = NSString(format:"%.2f",task.interval4) as String;
            var tmpDouble5:NSString = NSString(format:"%.2f",task.interval5) as String;
            var tmpDouble6:NSString = NSString(format:"%.2f",task.interval6) as String;
            var tmpDouble7:NSString = NSString(format:"%.2f",task.interval7) as String;
            var tmpDouble8:NSString = NSString(format:"%.2f",task.interval8) as String;
            var tmpDouble9:NSString = NSString(format:"%.2f",task.interval9) as String;
            cell.backgroundColor = UIColor.clearColor()
            let cellSelectedBgView = UIView()
            cellSelectedBgView.backgroundColor = UIColor.clearColor()
            cell.selectedBackgroundView = cellSelectedBgView
            cell.contentView.backgroundColor = UIColor.clearColor()
            cell.textLabel?.numberOfLines = 0
            cell.textLabel?.font = UIFont(name: "IPAexMincho", size: 20)
            cell.textLabel?.textColor = UIColor.whiteColor()
//            cell.layer.borderColor = UIColor.grayColor().CGColor
//            cell.layer.borderWidth = 3;
            cell.textLabel?.text = "\n     " + task.desc + "\n\n    1  -   2  :  " + (tmpDouble1 as String) + "\n    2  -   3  :  " + (tmpDouble2 as String) + "\n    3  -   4  :  " + (tmpDouble3 as String)  + "\n    4  -   5  :  " + (tmpDouble4 as String) + "\n    5  -   6  :  " + (tmpDouble5 as String) + "\n    6  -   7  :  " + (tmpDouble6 as String) + "\n    7  -   8  :  " + (tmpDouble7 as String) + "\n    8  -   9  :  " + (tmpDouble8 as String) + "\n    9  -  10 :  " + (tmpDouble9 as String)
            return cell
    }

    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        let managedObject:NSManagedObject = fetchedResultController.objectAtIndexPath(indexPath) as! NSManagedObject
        managedObjectContext?.deleteObject(managedObject)
        do {
            try managedObjectContext?.save()
        } catch _ {
        }
    }

    func controllerDidChangeContent(controller: NSFetchedResultsController) {
        tableView.reloadData()
    }
    
    override func tableView(tableView: UITableView?, didSelectRowAtIndexPath indexPath:NSIndexPath) {
        let cell: UITableViewCell = tableView!.cellForRowAtIndexPath(indexPath)!
        if selectedCount < 2 {
            let image:UIImage = UIImage(named:"checkmark")!
            let imageView:UIImageView = UIImageView(image: image)
            imageView.frame = CGRectMake(0, 0, 20, 20)
            cell.accessoryView = imageView
            if selectedCell1.isEmpty{
                selectedCell1 = cell.textLabel!.text!
            } else if selectedCell2.isEmpty {
                selectedCell2 = cell.textLabel!.text!
            }
            
            self.delegate.message1 = selectedCell1
            self.delegate.message2 = selectedCell2
//            let resultManager:ResultViewController = ResultViewController()
//            resultManager.firstCell = selectedCell1
//            resultManager.secondCell = selectedCell2
            if !selectedCell1.isEmpty && !selectedCell2.isEmpty {
                let pattern = "\\d+\\.\\d+"
                let regex = try? NSRegularExpression(pattern: pattern, options: NSRegularExpressionOptions.CaseInsensitive)
                
                let matches1=regex!.matchesInString(selectedCell1, options: [], range:NSMakeRange(0,  selectedCell1.characters.count))
                for match1 in matches1 {
                    selectedArray1.append((selectedCell1 as NSString).substringWithRange(match1.range))
                }
                let matches2=regex!.matchesInString(selectedCell2, options: [], range:NSMakeRange(0,  selectedCell2.characters.count))
                for match2 in matches2 {
                    selectedArray2.append((selectedCell2 as NSString).substringWithRange(match2.range))
                }
                if atof(selectedArray1[0]) > atof(selectedArray2[0]) {
                    synchro1 = (atof(selectedArray2[0])/atof(selectedArray1[0]))*10*10/9
                } else if selectedArray1[0] < selectedArray2[0]{
                    synchro1 = (atof(selectedArray1[0])/atof(selectedArray2[0]))*10*10/9
                } else if selectedArray1[0] == selectedArray2[0]{
                    synchro1 = 10*10/9
                }
                if atof(selectedArray1[1]) > atof(selectedArray2[1]) {
                    synchro2 = (atof(selectedArray2[1])/atof(selectedArray1[1]))*10*10/9
                } else if selectedArray1[1] < selectedArray2[1]{
                    synchro2 = (atof(selectedArray1[1])/atof(selectedArray2[1]))*10*10/9
                } else if selectedArray1[1] == selectedArray2[1]{
                    synchro2 = 10*10/9
                }
                if atof(selectedArray1[2]) > atof(selectedArray2[2]) {
                    synchro3 = (atof(selectedArray2[2])/atof(selectedArray1[2]))*10*10/9
                } else if selectedArray1[2] < selectedArray2[2]{
                    synchro3 = (atof(selectedArray1[2])/atof(selectedArray2[2]))*10*10/9
                } else if selectedArray1[2] == selectedArray2[2]{
                    synchro3 = 10*10/9
                }
                if atof(selectedArray1[3]) > atof(selectedArray2[3]) {
                    synchro4 = (atof(selectedArray2[3])/atof(selectedArray1[3]))*10*10/9
                } else if selectedArray1[3] < selectedArray2[3]{
                    synchro4 = (atof(selectedArray1[3])/atof(selectedArray2[3]))*10*10/9
                } else if selectedArray1[3] == selectedArray2[3]{
                    synchro4 = 10*10/9
                }
                if atof(selectedArray1[4]) > atof(selectedArray2[4]) {
                    synchro5 = (atof(selectedArray2[4])/atof(selectedArray1[4]))*10*10/9
                } else if selectedArray1[4] < selectedArray2[4]{
                    synchro5 = (atof(selectedArray1[4])/atof(selectedArray2[4]))*10*10/9
                } else if selectedArray1[4] == selectedArray2[4]{
                    synchro5 = 10*10/9
                }
                if atof(selectedArray1[5]) > atof(selectedArray2[5]) {
                    synchro6 = (atof(selectedArray2[5])/atof(selectedArray1[5]))*10*10/9
                } else if selectedArray1[5] < selectedArray2[5]{
                    synchro6 = (atof(selectedArray1[5])/atof(selectedArray2[5]))*10*10/9
                } else if selectedArray1[5] == selectedArray2[5]{
                    synchro6 = 10*10/9
                }
                if atof(selectedArray1[6]) > atof(selectedArray2[6]) {
                    synchro7 = (atof(selectedArray2[6])/atof(selectedArray1[6]))*10*10/9
                } else if selectedArray1[6] < selectedArray2[6]{
                    synchro7 = (atof(selectedArray1[6])/atof(selectedArray2[6]))*10*10/9
                } else if selectedArray1[6] == selectedArray2[6]{
                    synchro7 = 10*10/9
                }
                if atof(selectedArray1[7]) > atof(selectedArray2[7]) {
                    synchro8 = (atof(selectedArray2[7])/atof(selectedArray1[7]))*10*10/9
                } else if selectedArray1[7] < selectedArray2[7]{
                    synchro8 = (atof(selectedArray1[7])/atof(selectedArray2[7]))*10*10/9
                } else if selectedArray1[7] == selectedArray2[7]{
                    synchro8 = 10*10/9
                }
                if atof(selectedArray1[8]) > atof(selectedArray2[8]) {
                    synchro9 = (atof(selectedArray2[8])/atof(selectedArray1[8]))*10*10/9
                } else if selectedArray1[8] < selectedArray2[8]{
                    synchro9 = (atof(selectedArray1[8])/atof(selectedArray2[8]))*10*10/9
                } else if selectedArray1[8] == selectedArray2[8]{
                    synchro9 = 10*10/9
                }
                synchroPersent = synchro1 + synchro2 + synchro3 + synchro4 + synchro5 + synchro6 + synchro7 + synchro8 + synchro9
                let adjustedSynchroPersent:Double = atof(NSString(format:"%.2f",synchroPersent) as String)
                globalAdjustedSynchroPersent = adjustedSynchroPersent
                resultLabel.setTitle("マッチング結果 ☞", forState: .Normal)
                resultLabel.backgroundColor = UIColor(red: 0.9, green: 0.9, blue: 0.9, alpha: 0.2)
                resultLabel.layer.cornerRadius = 5
                selectedArray1 = []
                selectedArray2 = []
            }
            selectedCount++
        }
    }

    override func tableView(tableView: UITableView?, didDeselectRowAtIndexPath indexPath:NSIndexPath) {
        let cell: UITableViewCell = tableView!.cellForRowAtIndexPath(indexPath)!
        cell.accessoryView = nil
        if cell.textLabel!.text! == selectedCell1{
            selectedCell1 = ""
            selectedCount--
        } else if cell.textLabel!.text! == selectedCell2{
            selectedCell2 = ""
            selectedCount--
        }
        resultLabel.setTitle("", forState: .Normal)
        resultLabel.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0)
    }
    func onAdStart() {
        let myBoundSize: CGSize = UIScreen.mainScreen().bounds.size
        nadView.removeFromSuperview()
        nadView = NADView(frame: CGRect(x: myBoundSize.width/2-160, y: myBoundSize.height-50, width: 320, height: 50))
        nadView.setNendID("3df85edb7a0b0b30ed826a296a0c0881a8612fe1",
            spotID: "492647")
        nadView.isOutputLog = false
        nadView.delegate = self
        nadView.load()
        self.view.addSubview(nadView)
    }
    
    override func viewDidAppear(animated: Bool) {
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "onOrientationChange:", name: UIDeviceOrientationDidChangeNotification, object: nil)
    }
    
    // 端末の向きがかわったら呼び出される.
    func onOrientationChange(notification: NSNotification){
        let myBoundSize: CGSize = UIScreen.mainScreen().bounds.size
        nadView.removeFromSuperview()
        nadView = NADView(frame: CGRect(x: myBoundSize.width/2-160, y: myBoundSize.height-50, width: 320, height: 50))
        nadView.setNendID("3df85edb7a0b0b30ed826a296a0c0881a8612fe1",
            spotID: "492647")
        nadView.isOutputLog = false
        nadView.delegate = self
        nadView.load()
        self.view.addSubview(nadView)
    }
}
