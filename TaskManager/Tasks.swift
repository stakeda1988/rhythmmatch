//
//  Tasks.swift
//  TaskManager
//
//  Created by Ravi Shankar on 12/07/14.
//  Copyright (c) 2014 Ravi Shankar. All rights reserved.
//

import Foundation
import CoreData

class Tasks: NSManagedObject {

    @NSManaged var desc: String
    @NSManaged var interval1: Double
    @NSManaged var interval2: Double
    @NSManaged var interval3: Double
    @NSManaged var interval4: Double
    @NSManaged var interval5: Double
    @NSManaged var interval6: Double
    @NSManaged var interval7: Double
    @NSManaged var interval8: Double
    @NSManaged var interval9: Double

}
