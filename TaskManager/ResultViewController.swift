//
//  ResultViewController.swift
//  TaskManager
//
//  Created by SHOKI TAKEDA on 10/9/15.
//  Copyright (c) 2015 Ravi Shankar. All rights reserved.
//

import UIKit
import CoreData
import iAd

class ResultViewController: UIViewController, NADViewDelegate {
//    @IBOutlet weak var secondTouchLabel: UILabel!
//    @IBOutlet weak var firstTouchLabel: UILabel!
    @IBOutlet weak var syncroResultLabel: UILabel!
    @IBOutlet weak var secondNameLabel: UILabel!
    @IBOutlet weak var firstNameLabel: UILabel!
    @IBOutlet weak var backgroundView: UIImageView!
    @IBOutlet weak var jobSynchroLabel: UILabel!
    @IBOutlet weak var jobMessageLabel: UILabel!
    @IBOutlet weak var resultSynchroLabel: UILabel!
    @IBOutlet weak var resultMessageLabel: UILabel!
    private var nadView: NADView!
    var resultSynchroRate:Double!
    var firstCell: String = ""
//    var firstTouchArray:[String] = []
    var secondCell: String = ""
    var adTimer:NSTimer!
//    var secondTouchArray:[String] = []
    override func viewDidLoad() {
        let myBoundSize: CGSize = UIScreen.mainScreen().bounds.size
        nadView = NADView(frame: CGRect(x: myBoundSize.width/2-160, y: myBoundSize.height-50, width: 320, height: 50))
        nadView.setNendID("3df85edb7a0b0b30ed826a296a0c0881a8612fe1",
            spotID: "492647")
        nadView.isOutputLog = false
        nadView.delegate = self
        nadView.load()
        self.view.addSubview(nadView)
        adTimer = NSTimer.scheduledTimerWithTimeInterval(10.0, target: self, selector: Selector("onAdStart"), userInfo: nil, repeats: true)
//        var resultTaskViewManager:TaskManagerViewController = TaskManagerViewController()
        var delegate:AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
//        firstNameLabel.text = resultTaskManager.selectedCell1
        firstNameLabel.text = delegate.message1
//        firstTouchLabel.text = "    1  -   2  :  " + (resultTaskManager.selectedArray1[0] as String) + "\n    2  -   3  :  " + (resultTaskManager.selectedArray1[1] as String) + "\n    3  -   4  :  " + (resultTaskManager.selectedArray1[2] as String)  + "\n    4  -   5  :  " + (resultTaskManager.selectedArray1[3] as String) + "\n    5  -   6  :  " + (resultTaskManager.selectedArray1[4] as String) + "\n    6  -   7  :  " + (resultTaskManager.selectedArray1[5] as String) + "\n    7  -   8  :  " + (resultTaskManager.selectedArray1[6] as String) + "\n    8  -   9  :  " + (resultTaskManager.selectedArray1[7] as String) + "\n    9  -  10 :  " + (resultTaskManager.selectedArray1[8] as String)
//        secondNameLabel.text = secondCell
        secondNameLabel.text = delegate.message2
//        secondTouchLabel.text = "    1  -   2  :  " + (resultTaskManager.selectedArray2[0] as String) + "\n    2  -   3  :  " + (resultTaskManager.selectedArray1[1] as String) + "\n    3  -   4  :  " + (resultTaskManager.selectedArray1[2] as String)  + "\n    4  -   5  :  " + (resultTaskManager.selectedArray2[3] as String) + "\n    5  -   6  :  " + (resultTaskManager.selectedArray2[4] as String) + "\n    6  -   7  :  " + (resultTaskManager.selectedArray2[5] as String) + "\n    7  -   8  :  " + (resultTaskManager.selectedArray2[6] as String) + "\n    8  -   9  :  " + (resultTaskManager.selectedArray2[7] as String) + "\n    9  -  10 :  " + (resultTaskManager.selectedArray2[8] as String)
        resultSynchroLabel.text = NSString(format: "%.2f", resultSynchroRate) as String
        resultMessageLabel.numberOfLines = 0
        jobMessageLabel.numberOfLines = 0
        resultMessageLabel.sizeToFit()
        jobMessageLabel.sizeToFit()
        syncroResultLabel.text = String("\(resultSynchroRate)")
        if (resultSynchroRate < 10){
            self.navigationController?.navigationBar.barTintColor = UIColor(red: 246/255, green: 241/255, blue: 227/255, alpha: 0.5)
            self.navigationController?.navigationBar.tintColor = UIColor.brownColor()
            resultSynchroLabel.text = "40"
            jobSynchroLabel.text = "65"
            resultMessageLabel.text = "何を考えているのか意味が分らない所に惹かれ合っちゃう間柄です。付き合えますが、破局した場合は絶縁になる２人です。恋でしか繋がれません。"
            jobMessageLabel.text = "その人はあなたにとって、自分には無いアイディアの宝庫です。仕事なら、このくらいズレた相手とチームを組むと良い企画が生まれます。マンツーマンで友情を育むにはイマイチですが、グループで遊ぶと楽しい間柄です。"
            let tmpBackgroundView = UIImage(named:"01.jpg")
            backgroundView.image = tmpBackgroundView
        } else if(resultSynchroRate >= 10 && resultSynchroRate < 20){
            self.navigationController?.navigationBar.barTintColor = UIColor(red: 246/255, green: 241/255, blue: 227/255, alpha: 0.5)
            self.navigationController?.navigationBar.tintColor = UIColor.brownColor()
            resultSynchroLabel.text = "40"
            jobSynchroLabel.text = "65"
            resultMessageLabel.text = "何を考えているのか意味が分らない所に惹かれ合っちゃう間柄です。付き合えますが、破局した場合は絶縁になる２人です。恋でしか繋がれません。"
            jobMessageLabel.text = "その人はあなたにとって、自分には無いアイディアの宝庫です。仕事なら、このくらいズレた相手とチームを組むと良い企画が生まれます。マンツーマンで友情を育むにはイマイチですが、グループで遊ぶと楽しい間柄です。"
            let tmpBackgroundView = UIImage(named:"01.jpg")
            backgroundView.image = tmpBackgroundView
        } else if(resultSynchroRate >= 20 && resultSynchroRate < 30){
            self.navigationController?.navigationBar.barTintColor = UIColor(red: 246/255, green: 241/255, blue: 227/255, alpha: 0.5)
            self.navigationController?.navigationBar.tintColor = UIColor.brownColor()
            resultSynchroLabel.text = "70"
            jobSynchroLabel.text = "65"
            resultMessageLabel.text = "何を考えているのか意味が分らない所に惹かれ合っちゃう間柄です。付き合えますが、破局した場合は絶縁になる２人です。恋でしか繋がれません。"
            jobMessageLabel.text = "その人はあなたにとって、自分には無いアイディアの宝庫です。仕事なら、このくらいズレた相手とチームを組むと良い企画が生まれます。マンツーマンで友情を育むにはイマイチですが、グループで遊ぶと楽しい間柄です。"
            let tmpBackgroundView = UIImage(named:"01.jpg")
            backgroundView.image = tmpBackgroundView
        } else if(resultSynchroRate >= 30 && resultSynchroRate < 40){
            self.navigationController?.navigationBar.barTintColor = UIColor(red: 246/255, green: 241/255, blue: 227/255, alpha: 0.5)
            self.navigationController?.navigationBar.tintColor = UIColor.brownColor()
            resultSynchroLabel.text = "80"
            jobSynchroLabel.text = "30"
            resultMessageLabel.text = "何を考えているのか意味が分らない所に惹かれ合っちゃう間柄です。付き合えますが、破局した場合は絶縁になる２人です。恋でしか繋がれません。"
            jobMessageLabel.text = "その人はあなたにとって、自分には無いアイディアの宝庫です。仕事なら、このくらいズレた相手とチームを組むと良い企画が生まれます。マンツーマンで友情を育むにはイマイチですが、グループで遊ぶと楽しい間柄です。"
            let tmpBackgroundView = UIImage(named:"03.jpg")
            backgroundView.image = tmpBackgroundView
        } else if(resultSynchroRate >= 40 && resultSynchroRate < 50){
            self.navigationController?.navigationBar.barTintColor = UIColor(red: 246/255, green: 241/255, blue: 227/255, alpha: 0.5)
            self.navigationController?.navigationBar.tintColor = UIColor.brownColor()
            resultSynchroLabel.text = "100"
            jobSynchroLabel.text = "30"
            resultMessageLabel.text = "結婚までイケるレベルです。"
            jobMessageLabel.text = "今後これ以上仲良くなることも、仲がこじれるようなことも、ありません。末永く今の感じで付き合っていけます。"
            let tmpBackgroundView = UIImage(named:"03.jpg")
            backgroundView.image = tmpBackgroundView
        } else if(resultSynchroRate >= 50 && resultSynchroRate < 60){
            self.navigationController?.navigationBar.barTintColor = UIColor(red: 246/255, green: 241/255, blue: 227/255, alpha: 0.5)
            self.navigationController?.navigationBar.tintColor = UIColor.brownColor()
            resultSynchroLabel.text = "90"
            jobSynchroLabel.text = "30"
            resultMessageLabel.text = "結婚までイケるレベルです。"
            jobMessageLabel.text = "今後これ以上仲良くなることも、仲がこじれるようなことも、ありません。末永く今の感じで付き合っていけます。"
            let tmpBackgroundView = UIImage(named:"04.jpg")
            backgroundView.image = tmpBackgroundView
        } else if(resultSynchroRate >= 60 && resultSynchroRate < 70){
            self.navigationController?.navigationBar.barTintColor = UIColor(red: 246/255, green: 241/255, blue: 227/255, alpha: 0.5)
            self.navigationController?.navigationBar.tintColor = UIColor.brownColor()
            resultSynchroLabel.text = "90"
            jobSynchroLabel.text = "30"
            resultMessageLabel.text = "結婚までイケるレベルです。"
            jobMessageLabel.text = "今後これ以上仲良くなることも、仲がこじれるようなことも、ありません。末永く今の感じで付き合っていけます。"
            let tmpBackgroundView = UIImage(named:"04.jpg")
            backgroundView.image = tmpBackgroundView
        } else if(resultSynchroRate >= 70 && resultSynchroRate < 80){
            self.navigationController?.navigationBar.barTintColor = UIColor(red: 246/255, green: 241/255, blue: 227/255, alpha: 0.5)
            self.navigationController?.navigationBar.tintColor = UIColor.brownColor()
            resultSynchroLabel.text = "80"
            jobSynchroLabel.text = "80"
            resultMessageLabel.text = "似た者同士なので恋には落ちづらいですが、付き合って破局した場合に、友達になることが出来る貴重な相性です。恋が失われても、良い関係が続きます。"
            jobMessageLabel.text = "わりと似たような思考の持ち主です。お互いの言動の深意を、ごく普通に理解出来るので、すれ違いが起きづらく問題も起きづらいです。"
            let tmpBackgroundView = UIImage(named:"04.jpg")
            backgroundView.image = tmpBackgroundView
        } else if(resultSynchroRate >= 80 && resultSynchroRate < 90){
            self.navigationController?.navigationBar.barTintColor = UIColor(red: 246/255, green: 241/255, blue: 227/255, alpha: 0.5)
            self.navigationController?.navigationBar.tintColor = UIColor.brownColor()
            resultSynchroLabel.text = "30"
            jobSynchroLabel.text = "80"
            resultMessageLabel.text = "似た者同士なので恋には落ちづらいですが、付き合って破局した場合に、友達になることが出来る貴重な相性です。恋が失われても、良い関係が続きます。"
            jobMessageLabel.text = "わりと似たような思考の持ち主です。お互いの言動の深意を、ごく普通に理解出来るので、すれ違いが起きづらく問題も起きづらいです。"
            let tmpBackgroundView = UIImage(named:"05.jpg")
            backgroundView.image = tmpBackgroundView
        } else if(resultSynchroRate >= 90 && resultSynchroRate < 96){
            self.navigationController?.navigationBar.barTintColor = UIColor(red: 246/255, green: 241/255, blue: 227/255, alpha: 0.5)
            self.navigationController?.navigationBar.tintColor = UIColor.brownColor()
            resultSynchroLabel.text = "60"
            jobSynchroLabel.text = "98"
            resultMessageLabel.text = "似た者同士なので恋には落ちづらいですが、付き合って破局した場合に、友達になることが出来る貴重な相性です。恋が失われても、良い関係が続きます。"
            jobMessageLabel.text = "友達なら、親友になれます。\n仕事をする間柄なら、お互いの右腕になれます。"
            let tmpBackgroundView = UIImage(named:"05.jpg")
            backgroundView.image = tmpBackgroundView
        } else if(resultSynchroRate >= 96 && resultSynchroRate < 100){
            self.navigationController?.navigationBar.barTintColor = UIColor(red: 246/255, green: 241/255, blue: 227/255, alpha: 0.5)
            self.navigationController?.navigationBar.tintColor = UIColor.brownColor()
            resultSynchroLabel.text = "50"
            jobSynchroLabel.text = "120"
            resultMessageLabel.text = "似た者同士なので恋には落ちづらいですが、付き合って破局した場合に、友達になることが出来る貴重な相性です。恋が失われても、良い関係が続きます。"
            jobMessageLabel.text = "ほぼ自分です。というか、自分以上に自分です、その人はあなたにとって。似てるどころの騒ぎじゃありません。"
            let tmpBackgroundView = UIImage(named:"05.jpg")
            backgroundView.image = tmpBackgroundView
        }
    }
    func onAdStart() {
        let myBoundSize: CGSize = UIScreen.mainScreen().bounds.size
        nadView.removeFromSuperview()
        nadView = NADView(frame: CGRect(x: myBoundSize.width/2-160, y: myBoundSize.height-50, width: 320, height: 50))
        nadView.setNendID("3df85edb7a0b0b30ed826a296a0c0881a8612fe1",
            spotID: "492647")
        nadView.isOutputLog = false
        nadView.delegate = self
        nadView.load()
        self.view.addSubview(nadView)
    }
    
    override func viewDidAppear(animated: Bool) {
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "onOrientationChange:", name: UIDeviceOrientationDidChangeNotification, object: nil)
    }
    
    // 端末の向きがかわったら呼び出される.
    func onOrientationChange(notification: NSNotification){
        let myBoundSize: CGSize = UIScreen.mainScreen().bounds.size
        nadView.removeFromSuperview()
        nadView = NADView(frame: CGRect(x: myBoundSize.width/2-160, y: myBoundSize.height-50, width: 320, height: 50))
        nadView.setNendID("3df85edb7a0b0b30ed826a296a0c0881a8612fe1",
            spotID: "492647")
        nadView.isOutputLog = false
        nadView.delegate = self
        nadView.load()
        self.view.addSubview(nadView)
    }
}