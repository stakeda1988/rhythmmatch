//
//  TaskDetailViewController.swift
//  TaskManager
//
//  Created by Ravi Shankar on 12/07/14.
//  Copyright (c) 2014 Ravi Shankar. All rights reserved.
//

import UIKit
import CoreData
import iAd

class TaskDetailViewController: UIViewController, NADViewDelegate {
    @IBOutlet var txtDesc: UITextField!
    @IBOutlet weak var orderLabel: UILabel!

    @IBAction func tapScreen(sender: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
    private var nadView: NADView!
    var counter:Int = 0
    var array = [NSDate]()
    var lagArray = [NSTimeInterval]()
    var localtime1:String!
    var localtime2:String!
    var localtime3:String!
    var localtime4:String!
    var localtime5:String!
    var localtime6:String!
    var localtime7:String!
    var localtime8:String!
    var localtime9:String!
    var task: Tasks? = nil
    var adTimer:NSTimer!
    let managedObjectContext = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        self.canDisplayBannerAds = true
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 246/255, green: 241/255, blue: 227/255, alpha: 0.5)
        if task != nil {
            txtDesc.text = task?.desc
        }
        let myBoundSize: CGSize = UIScreen.mainScreen().bounds.size
        nadView = NADView(frame: CGRect(x: myBoundSize.width/2-160, y: myBoundSize.height-50, width: 320, height: 50))
        nadView.setNendID("3df85edb7a0b0b30ed826a296a0c0881a8612fe1",
            spotID: "492647")
        nadView.isOutputLog = false
        nadView.delegate = self
        nadView.load()
        self.view.addSubview(nadView)
        
        adTimer = NSTimer.scheduledTimerWithTimeInterval(10.0, target: self, selector: Selector("onAdStart"), userInfo: nil, repeats: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func done(sender: AnyObject) {
        createTask()
        dismissViewController()
    }
    
    @IBAction func cancel(sender: AnyObject) {
         dismissViewController()
    }
    
    func dismissViewController() {
        navigationController?.popViewControllerAnimated(true)
    }

    func createTask() {
        let entityDescripition = NSEntityDescription.entityForName("Tasks", inManagedObjectContext: managedObjectContext!)
        let task = Tasks(entity: entityDescripition!, insertIntoManagedObjectContext: managedObjectContext)
        task.desc = txtDesc.text!
        task.interval1 = atof(NSString(format:"%.2f",lagArray[0]) as String)
        task.interval2 = atof(NSString(format:"%.2f",lagArray[1]) as String)
        task.interval3 = atof(NSString(format:"%.2f",lagArray[2]) as String)
        task.interval4 = atof(NSString(format:"%.2f",lagArray[3]) as String)
        task.interval5 = atof(NSString(format:"%.2f",lagArray[4]) as String)
        task.interval6 = atof(NSString(format:"%.2f",lagArray[5]) as String)
        task.interval7 = atof(NSString(format:"%.2f",lagArray[6]) as String)
        task.interval8 = atof(NSString(format:"%.2f",lagArray[7]) as String)
        task.interval9 = atof(NSString(format:"%.2f",lagArray[8]) as String)
        do {
            try managedObjectContext?.save()
        } catch _ {
        }
    }
    
    @IBOutlet weak var countLabel: UILabel!
    @IBOutlet weak var resultLabel: UILabel!
    @IBOutlet weak var resultDataLabel: UILabel!
    @IBAction func countTouch(sender: AnyObject) {
        counter++
        
        let tmpDate = NSDate()
        array.append(tmpDate)
        var intervalBefore:NSDate
        var intervalAfter:NSDate
        var intervalTime:NSTimeInterval
        if counter < 10 {
            countLabel.text = String(counter)
            if counter == 1 {
            } else {
                intervalBefore = array[counter-2]
                intervalAfter = array[counter-1]
                intervalTime = intervalAfter.timeIntervalSinceDate(intervalBefore)
                lagArray.append(intervalTime)
            }
        } else {
            intervalBefore = array[counter-2]
            intervalAfter = array[counter-1]
            intervalTime = intervalAfter.timeIntervalSinceDate(intervalBefore)
            lagArray.append(intervalTime)
            countLabel.text = "10"
            resultLabel.text = "終了です！お疲れさまでした。"
            localtime1 = NSString(format:"%.2f",lagArray[0]) as String;
            localtime2 = NSString(format:"%.2f",lagArray[1]) as String;
            localtime3 = NSString(format:"%.2f",lagArray[2]) as String;
            localtime4 = NSString(format:"%.2f",lagArray[3]) as String;
            localtime5 = NSString(format:"%.2f",lagArray[4]) as String;
            localtime6 = NSString(format:"%.2f",lagArray[5]) as String;
            localtime7 = NSString(format:"%.2f",lagArray[6]) as String;
            localtime8 = NSString(format:"%.2f",lagArray[7]) as String;
            localtime9 = NSString(format:"%.2f",lagArray[8]) as String;
            resultDataLabel.text = "1  -  2 : " + localtime1 + "\n" + "2  -  3 : " + localtime2 + "\n" + "3  -  4 : " + localtime3 + "\n" + "4  -  5 : " + localtime4 + "\n" + "5  -  6 : " + localtime5 + "\n" + "6  -  7 : " + localtime6 + "\n" + "7  -  8 : " + localtime7 + "\n" + "8  -  9 : " + localtime8 + "\n" + "9  -10 : " + localtime9 + "\n"
        }
    }
    func onAdStart() {
        let myBoundSize: CGSize = UIScreen.mainScreen().bounds.size
        nadView.removeFromSuperview()
        nadView = NADView(frame: CGRect(x: myBoundSize.width/2-160, y: myBoundSize.height-50, width: 320, height: 50))
        nadView.setNendID("3df85edb7a0b0b30ed826a296a0c0881a8612fe1",
            spotID: "492647")
        nadView.isOutputLog = false
        nadView.delegate = self
        nadView.load()
        self.view.addSubview(nadView)
    }
    
    override func viewDidAppear(animated: Bool) {
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "onOrientationChange:", name: UIDeviceOrientationDidChangeNotification, object: nil)
    }
    
    // 端末の向きがかわったら呼び出される.
    func onOrientationChange(notification: NSNotification){
        let myBoundSize: CGSize = UIScreen.mainScreen().bounds.size
        nadView.removeFromSuperview()
        nadView = NADView(frame: CGRect(x: myBoundSize.width/2-160, y: myBoundSize.height-50, width: 320, height: 50))
        nadView.setNendID("3df85edb7a0b0b30ed826a296a0c0881a8612fe1",
            spotID: "492647")
        nadView.isOutputLog = false
        nadView.delegate = self
        nadView.load()
        self.view.addSubview(nadView)
    }
}
